# Velox-starter spring boot package

## Introduction
This package is meant to implement crosscutting aspects like security or logging which are
omnipresent in multiple Velox services.

## Features

### Security
Provided is preconfigured method security with authorization using OIDC authentication.

#### Okta
To use Okta, the following properties need to be used:
```
velox:
  security:
    oidc:
      enabled: true
      claim:
        grantedAuthority: "groups"
spring:
  security:
    oauth2:
      client:
        registration:
          okta:
            client-id: 0oaawlq15FBSjkb424x6
        provider:
          okta:
            issuer-uri: https://dev-712077.okta.com/oauth2/default
            client-id: 0oaawlq15FBSjkb424x6
      resourceserver:
        jwt:
          issuer-uri: https://dev-712077.okta.com/oauth2/default
```
#### Zitadel
To use Zitadel, the following properties need to be used:
```
velox:
  security:
    oidc:
      enabled: true
      claim:
        grantedAuthority: "urn:zitadel:iam:org:project:roles"
spring:
  security:
    oauth2:
      client:
        registration:
          zitadel:
            client-id: 195456342570369281@velox_local
        provider:
          zitadel:
            issuer-uri: https://velox-nkuf8w.zitadel.cloud
            client-id: 195456342570369281@velox_local
      resourceserver:
        jwt:
          issuer-uri: https://velox-nkuf8w.zitadel.cloud
        opaquetoken:
          introspection-uri: https://velox-nkuf8w.zitadel.cloud/oauth/v2/introspect
          client-id: 197217175780524289@velox_local
          client-secret: must_be_overwritten_by_runtime_configuration
```

The client secret which is used for opaque token introspection must be provided at runtime either
with a program argument (--spring.security.oauth2.resourceserver.opaquetoken.client-secret) or as
an environment variable (SPRING_SECURITY_OAUTH2_RESOURCESERVER_OPAQUETOKEN_CLIENT_SECRET).

#### Local
Alternatively, eg. for testing purposes, this can be disabled, falling back to local authentication using
`velox.security.oidc.enabled` property.
In this case there are three predefined users in *VeloxLocalWebSecurityConfiguration.java*:

| user       | password | assigned authority |
|------------|----------|--------------------|
| user       | velox    | User               |
| other_user | velox    | User               |
| admin      | velox    | Admin              |
| cart_admin | velox    | Admin_Cart         |
 
#### Provided functions
Initial solution provides *owner* authority for entities. This is done with
*shop.velox.commons.security.annotation.OwnerId* annotation used to identify entity's owner getter
and relevant **'owner'** permission.
Security *AuthorizationEvaluator* providing convenience methods is also available.

Refer to *shop.velox.cart.dao.CartRepository* in velox-shop/cart> for example usages.

### Mail
Common mail configuration and implementation. **mailService** bean is registered as single point of interaction.
Messages are built with chaining builders.

Configuration is done with Spring's *spring.mail.* and *velox.mail.* property trees.

#### Sending plain mail
**mailService** exposes *getSimpleMessageBuilder()* factory method. Returned builder offers methods to customize message before sending. Minimally, setting recipient is required to successfully send message.

Example usage:
```java
class MailExample{

  @Autowired
  private MailService mailService;

  void sendMessage() {
    MessageBuilder builder = mailService.getSimpleMessageBuilder();
    buider
      .addRecipient("My dear recipient <recipent@velox.shop>")
      .setSubject("Message for recipient")
      .setMailBody("This is my message content.")
      .send();
  }
}
```

## Adding starter to custom project
To enable starter project maven repository needs to be added.
In Gradle this is done by including following snippet in project's *build.gradle* file `repositories` section.

    maven {
        url "https://gitlab.com/api/v4/groups/velox-shop/-/packages/maven"
        name "Velox-GitLab"
    }

And adding it as a dependency eg.: `implementation('shop.velox:velox-starter:1.0.0-SNAPSHOT')`.
