package shop.velox.commons.mail.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import shop.velox.commons.mail.service.MailService;
import shop.velox.commons.mail.service.MessageBuilder;

@RequiredArgsConstructor
public class VeloxMailService implements MailService {

  private final JavaMailSender mailSender;
  private String sender;

  /**
   * {@inheritDoc}
   */
  @Override
  public MessageBuilder getSimpleMessageBuilder() {
    return SimpleMessageBuilder.createSimpleMessageBuilder(mailSender, sender);
  }

  public void setSender(String sender) {
    this.sender = sender;
  }
}
