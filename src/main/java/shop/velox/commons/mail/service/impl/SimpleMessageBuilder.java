package shop.velox.commons.mail.service.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import shop.velox.commons.mail.service.MessageBuilder;

/**
 * Basic implementation of {@link MessageBuilder} supporting plain text messages ({@link String}
 * only body)
 */
@Slf4j
public class SimpleMessageBuilder implements MessageBuilder {

  private final SimpleMailMessage simpleMailMessage;
  private JavaMailSender mailSender;
  private final HashSet<String> recipients = new HashSet<>();

  private SimpleMessageBuilder() {
    simpleMailMessage = new SimpleMailMessage();
  }

  private SimpleMessageBuilder(final JavaMailSender mailSender, final String defaultSender) {
    this();
    this.mailSender = mailSender;
    simpleMailMessage.setFrom(defaultSender);
    simpleMailMessage.setText("");
    log.info("Mails will have {} as default sender", defaultSender);
  }

  /**
   * {@inheritDoc}
   */
  public SimpleMessageBuilder setMailFrom(final String mailFrom) {
    simpleMailMessage.setFrom(mailFrom);
    return this;
  }

  /**
   * {@inheritDoc}
   */
  public SimpleMessageBuilder addRecipient(final String... recipient) {
    Optional.ofNullable(recipient).map(Arrays::asList).ifPresent(recipients::addAll);
    return this;
  }

  /**
   * {@inheritDoc}
   */
  public SimpleMessageBuilder setSubject(final String subject) {
    simpleMailMessage.setSubject(subject);
    return this;
  }

  /**
   * Sets message's plain text body if provided body is {@link String}. Passing null has no effect.
   *
   * @param mailBody plain text body to set
   * @return this builder for chaining
   */
  public SimpleMessageBuilder setMailBody(final Object mailBody) {
    Optional.ofNullable(mailBody).ifPresent(body -> simpleMailMessage.setText(
        Optional.of(body).filter(String.class::isInstance).map(String.class::cast).orElseThrow(
            () -> new IllegalArgumentException(
                this.getClass().getName() + " supports only String as message body."))));
    return this;
  }

  /**
   * {@inheritDoc}
   */
  public void send() {
    simpleMailMessage.setTo(recipients.toArray(String[]::new));
    mailSender.send(simpleMailMessage);
  }

  static SimpleMessageBuilder createSimpleMessageBuilder(final JavaMailSender mailSender,
      final String defaultSender) {
    return new SimpleMessageBuilder(mailSender, defaultSender);
  }
}
