package shop.velox.commons.logging.logback;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.contrib.json.classic.JsonLayout;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.Map;

@Setter
@Component
public class CustomJsonLayout extends JsonLayout {

  String appName;

  @Override
  protected void addCustomDataToJsonMap(Map<String, Object> map, ILoggingEvent event) {
    map.put("service", appName);
  }
}