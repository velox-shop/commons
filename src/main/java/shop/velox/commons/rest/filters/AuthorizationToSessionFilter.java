package shop.velox.commons.rest.filters;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
@Order(1)
@Slf4j
public class AuthorizationToSessionFilter implements Filter {

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) request;
    String authorizationHeader = req.getHeader(HttpHeaders.AUTHORIZATION);
    HttpSession session = req.getSession();
    session.setAttribute(HttpHeaders.AUTHORIZATION, authorizationHeader);
    chain.doFilter(request, response);
  }
}
