package shop.velox.commons.rest.filters;

import com.nimbusds.oauth2.sdk.util.StringUtils;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


/**
 * The RestTemplateHeaderModifierInterceptor has a task, with the help of
 * AuthorizationToSessionFilter to intercept the incoming HttpRequest to the controller, extract the
 * Authorization header that contains Authentication and Authorization information, and to append
 * Authorization header to the response before it is sent to client
 */

@Component
@Slf4j
public class RestTemplateHeaderModifierInterceptor implements ClientHttpRequestInterceptor {

  @Override
  public ClientHttpResponse intercept(HttpRequest request, byte[] body,
      ClientHttpRequestExecution execution) throws IOException {
    HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder
        .currentRequestAttributes()).getRequest();
    HttpSession session = httpServletRequest.getSession(false);
    String sessionAuth = (String) session.getAttribute(HttpHeaders.AUTHORIZATION);
    String authHeader = request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
    if (StringUtils.isBlank(authHeader)) {
      request.getHeaders().add(HttpHeaders.AUTHORIZATION, sessionAuth);
    }
    return execution.execute(request, body);
  }
}
