package shop.velox.commons.rest.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * The RestResponsePage class extends PageImpl class with @JsonCreator.
 * This class allows Jackson to instantiate API responses of Page<T> type.
 */

public class RestResponsePage<T> extends PageImpl<T> {

    private static final long serialVersionUID = 3248189030448292006L;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public RestResponsePage(@JsonProperty("content") List<T> content,
        @JsonProperty("number") int number,
        @JsonProperty("size") int size,
        @JsonProperty("totalElements") long totalElements,
        @JsonProperty("pageable") JsonNode pageable,
        @JsonProperty("last") boolean last,
        @JsonProperty("totalPages") int totalPages,
        @JsonProperty("sort") JsonNode sort,
        @JsonProperty("first") boolean first,
        @JsonProperty("numberOfElements") int numberOfElements) {
        super(content, createPageable(pageable, number, size, sort), totalElements);
    }

    public RestResponsePage(List<T> content, Pageable pageable, long total) {
        super(content, pageable, total);
        // TODO Auto-generated constructor stub
    }

    public RestResponsePage(List<T> content) {
        super(content);
        // TODO Auto-generated constructor stub
    }

    /* PageImpl does not have an empty constructor and this was causing an issue for RestTemplate to cast the Rest API response
     * back to Page.
     */
    public RestResponsePage() {
        super(new ArrayList<T>());
    }

    
    private static Pageable createPageable(JsonNode pageableNode, int number, int size,
        JsonNode sortNode) {
        if (pageableNode != null && pageableNode.has("unpaged") && pageableNode.get("unpaged")
            .asBoolean()) {
            return Pageable.unpaged();
        }

        Sort pageSort = Sort.unsorted();
        if (sortNode != null && !sortNode.isEmpty()) {
            List<Sort.Order> orders = new ArrayList<>();
            for (JsonNode orderNode : sortNode) {
                if (orderNode.has("property") && orderNode.has("direction")) {
                    String property = orderNode.get("property").asText();
                    String directionStr = orderNode.get("direction").asText();
                    Sort.Direction direction = Sort.Direction.fromString(directionStr);
                    orders.add(new Sort.Order(direction, property));
                }
            }
            if (!orders.isEmpty()) {
                pageSort = Sort.by(orders);
            }
        }

        return PageRequest.of(number, Math.max(1, size), pageSort);
    }

}
