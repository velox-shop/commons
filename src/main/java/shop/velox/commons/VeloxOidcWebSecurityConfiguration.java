package shop.velox.commons;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.server.resource.BearerTokenError;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationProvider;
import org.springframework.security.oauth2.server.resource.authentication.OpaqueTokenAuthenticationProvider;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.security.oauth2.server.resource.web.BearerTokenAuthenticationEntryPoint;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.MediaTypeRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.accept.ContentNegotiationStrategy;
import shop.velox.commons.security.CachingOpaqueTokenIntrospector;
import shop.velox.commons.security.VeloxNimbusOpaqueTokenIntrospector;

/**
 * OIDC based oAuth configuration
 */
@Configuration
@EnableWebSecurity
@Order(110)
@ConditionalOnProperty(value = "velox.security.oidc.enabled", havingValue = "true")
@RequiredArgsConstructor
@Slf4j
public class VeloxOidcWebSecurityConfiguration extends AbstractVeloxWebSecurityConfiguration {

  @Value("${velox.security.oidc.claim.grantedAuthority}")
  private String authoritiesClaimName;

  private final OAuth2ResourceServerProperties oAuth2ResourceServerProperties;

  private final JwtDecoder jwtDecoder;

  @Bean
  public AuthenticationManager authenticationManager(
      JwtAuthenticationProvider jwtAuthenticationProvider,
      OpaqueTokenAuthenticationProvider opaqueTokenAuthenticationProvider) {
    return new ProviderManager(jwtAuthenticationProvider, opaqueTokenAuthenticationProvider);
  }

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http,
      AuthenticationManager authenticationManager) throws Exception {
    commonConfiguration(http);
    http
        .oauth2ResourceServer(oauth2ResourceServer -> oauth2ResourceServer
            .jwt(Customizer.withDefaults()))
        .authenticationManager(authenticationManager);
    configureResourceServer401ResponseBody(http);
    return http.build();
  }

  @Bean
  public JwtAuthenticationProvider jwtAuthenticationProvider() {
    JwtAuthenticationProvider jwtAuthenticationProvider = new JwtAuthenticationProvider(jwtDecoder);
    jwtAuthenticationProvider.setJwtAuthenticationConverter(jwtAuthenticationConverter());
    return jwtAuthenticationProvider;
  }

  @Bean
  public OpaqueTokenAuthenticationProvider opaqueTokenAuthenticationProvider(
      @Value("${velox.security.oidc.opaquetoken.cache-expire-seconds:0}") int cacheExpireSeconds) {
    OpaqueTokenIntrospector delegate = new VeloxNimbusOpaqueTokenIntrospector(
        oAuth2ResourceServerProperties.getOpaquetoken().getIntrospectionUri(),
        oAuth2ResourceServerProperties.getOpaquetoken().getClientId(),
        oAuth2ResourceServerProperties.getOpaquetoken().getClientSecret(),
        authoritiesClaimName);
    OpaqueTokenIntrospector introspectionClient = new CachingOpaqueTokenIntrospector(delegate,
        cacheExpireSeconds);
    return new OpaqueTokenAuthenticationProvider(introspectionClient);
  }

  private JwtAuthenticationConverter jwtAuthenticationConverter() {
    // create a custom JWT converter to map the roles from the token as granted authorities
    ZitadelJwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new ZitadelJwtGrantedAuthoritiesConverter();
    jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName(authoritiesClaimName);
    jwtGrantedAuthoritiesConverter.setAuthorityPrefix(""); // default is: SCOPE_
    if (log.isInfoEnabled()) {
      log.info("Using JwtAuthenticationConverter: {}",
          ReflectionToStringBuilder.toString(jwtGrantedAuthoritiesConverter,
              ToStringStyle.JSON_STYLE));
    }

    JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
    jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);
    return jwtAuthenticationConverter;
  }

  public static void configureResourceServer401ResponseBody(HttpSecurity http)
      throws Exception {
    http.exceptionHandling(exceptionHandling -> exceptionHandling
        .defaultAuthenticationEntryPointFor(authenticationEntryPoint(), textRequestMatcher(http)));
  }

  private static AuthenticationEntryPoint authenticationEntryPoint() {
    BearerTokenAuthenticationEntryPoint bearerTokenEntryPoint = new BearerTokenAuthenticationEntryPoint();
    return (request, response, authException) -> {

      log.info("{}, {}", request, response, authException);

      response.setContentType(MediaType.TEXT_PLAIN.toString());
      response.getWriter().print(statusAsString(getStatus(authException)));
      bearerTokenEntryPoint.commence(request, response, authException);
    };
  }

  private static RequestMatcher textRequestMatcher(HttpSecurity http) {
    return new MediaTypeRequestMatcher(http.getSharedObject(ContentNegotiationStrategy.class),
        MediaType.TEXT_PLAIN);
  }

  static String statusAsString(HttpStatus status) {
    return status.value() + " " + status.getReasonPhrase();
  }

  static HttpStatus getStatus(AuthenticationException authException) {
    if (authException instanceof OAuth2AuthenticationException oAuth2AuthenticationException) {
      OAuth2Error error = oAuth2AuthenticationException.getError();
      if (error instanceof BearerTokenError bearerTokenError) {
        return bearerTokenError.getHttpStatus();
      }
    }
    return HttpStatus.UNAUTHORIZED;
  }

}
