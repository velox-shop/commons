package shop.velox.commons.utils;

import java.util.Optional;
import lombok.experimental.UtilityClass;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.web.util.UriComponentsBuilder;

@UtilityClass
public class PaginationUtils {

  public static void addPageableInformation(UriComponentsBuilder uriComponentsBuilder,
      @Nullable Pageable pageable) {
    Optional.ofNullable(pageable).ifPresent(p -> {
      uriComponentsBuilder.queryParam("page", p.getPageNumber())
          .queryParam("size", p.getPageSize());

      // Add sorting if present
      if (p.getSort().isSorted()) {
        p.getSort().forEach(order ->
            uriComponentsBuilder.queryParam("sort",
                order.getProperty() + "," + order.getDirection().name().toLowerCase())
        );
      }
    });
  }

}
