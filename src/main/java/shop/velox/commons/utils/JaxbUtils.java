package shop.velox.commons.utils;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Unmarshaller;
import java.io.StringReader;
import javax.xml.XMLConstants;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.xml.sax.InputSource;

@UtilityClass
public class JaxbUtils {

  /**
   * Unmarshal XML data to object.
   * <p>
   * See <a href="https://cheatsheetseries.owasp.org/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.html#jaxb-unmarshaller">OWASP Cheatsheet</a>
   *
   * @param context JAXB context
   * @param xmlData XML data
   * @return unmarshalled object
   */
  @SneakyThrows
  public static Object unmarshal(JAXBContext context, String xmlData) {
    SAXParserFactory spf = SAXParserFactory.newInstance();

    spf.setFeature("http://xml.org/sax/features/external-general-entities", false);
    spf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
    spf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
    spf.setXIncludeAware(false);

    //Do unmarshall operation
    Source xmlSource = new SAXSource(spf.newSAXParser().getXMLReader(),
        new InputSource(new StringReader(xmlData)));
    Unmarshaller um = context.createUnmarshaller();
    return um.unmarshal(xmlSource);
  }

}
