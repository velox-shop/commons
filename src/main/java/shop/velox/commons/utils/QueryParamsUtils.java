package shop.velox.commons.utils;

import java.nio.charset.StandardCharsets;
import lombok.experimental.UtilityClass;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.util.UriUtils;

@UtilityClass
public class QueryParamsUtils {

  /**
   * Adds encoded query parameters to the given {@link UriComponentsBuilder}.
   *
   * <p>This method manually encodes each query parameter value using
   * {@link UriUtils#encode(String, java.nio.charset.Charset)} with UTF-8 encoding before adding them
   * to the URI builder. This is necessary because the default behavior of
   * {@code UriComponentsBuilder} does not encode certain reserved characters (such as the plus sign, '+')
   * in query parameter values. According to RFC 3986 these characters are legal in the query component,
   * but many applications expect them to be percent‑encoded (for example, to prevent '+' from being interpreted
   * as a space).</p>
   *
   * <p>Note: When using this method, you should finalize the URI construction by calling
   * {@code build(true).toUri()} on the builder. Passing {@code true} to {@code build(boolean)} signals that
   * the URI components are already encoded, which prevents double-encoding of special characters.</p>
   *
   * @param uriComponentsBuilder the {@link UriComponentsBuilder} to which the query parameters will be added
   * @param queryParams a {@link MultiValueMap} of query parameter names and their corresponding values
   */
  public static void addQueryParams(UriComponentsBuilder uriComponentsBuilder,
      MultiValueMap<String, String> queryParams) {
    queryParams.forEach((key, values) -> {
      Object[] encodedValues = values.stream()
          .map(value -> UriUtils.encode(value, StandardCharsets.UTF_8))
          .toArray();
      uriComponentsBuilder.queryParam(key, encodedValues);
    });
  }

}
