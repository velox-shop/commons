package shop.velox.commons;

import static org.apache.commons.collections4.ListUtils.emptyIfNull;

import java.util.List;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import shop.velox.commons.security.LocalUser;

/**
 * Local HTTP Basic auth configuration used if OIDC authentication is not enabled
 */
@Configuration
@ConfigurationProperties(prefix = "velox.accounts")
@EnableWebSecurity
@Order(110)
@ConditionalOnProperty(value = "velox.security.oidc.enabled", havingValue = "false", matchIfMissing = true)
@Slf4j
public class VeloxLocalWebSecurityConfiguration extends AbstractVeloxWebSecurityConfiguration {

  public static final String LOCAL_ADMIN_USERNAME = "admin";
  public static final String LOCAL_ADMIN_PASSWORD = "velox";

  public static final String LOCAL_USER_1_USERNAME = "user";
  public static final String LOCAL_USER_1_PASSWORD = "velox";

  public static final String LOCAL_USER_2_USERNAME = "other_user";
  public static final String LOCAL_USER_2_PASSWORD = "velox";

  @Setter
  List<LocalUser> additionalUsers;

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    commonConfiguration(http)
        .httpBasic(Customizer.withDefaults());
    return http.build();
  }

  @Bean
  @ConditionalOnMissingBean(UserDetailsService.class)
  public UserDetailsService users() {

    log.info("Creating users for VeloxLocalWebSecurityConfiguration");

    UserDetails user = User.builder()
        .username(LOCAL_USER_1_USERNAME)
        .password(passwordEncoder().encode(LOCAL_USER_1_PASSWORD))
        .authorities("User")
        .build();

    UserDetails otherUser = User.builder()
        .username(LOCAL_USER_2_USERNAME)
        .password(passwordEncoder().encode(LOCAL_USER_2_PASSWORD))
        .authorities("User")
        .build();

    UserDetails admin = User.builder()
        .username(LOCAL_ADMIN_USERNAME)
        .password(passwordEncoder().encode(LOCAL_ADMIN_PASSWORD))
        .authorities("Admin")
        .build();

    UserDetails cartAdmin = User.builder()
        .username("cart_admin")
        .password(passwordEncoder().encode("velox"))
        .authorities("Admin_Cart")
        .build();

    var localUsers = emptyIfNull(additionalUsers)
        .stream()
        .map(localUser -> User.builder()
            .username(localUser.getUsername())
            .password(passwordEncoder().encode(localUser.getPassword()))
            .authorities(localUser.getAuthorities().toArray(new String[0]))
            .build())
        .toList();
    List<UserDetails> userDetails = ListUtils.union(localUsers,
        List.of(user, otherUser, admin, cartAdmin));

    log.info("VeloxLocalWebSecurityConfiguration creates: {}",
        userDetails.stream().map(UserDetails::getUsername).toList());

    return new InMemoryUserDetailsManager(userDetails);
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

}
