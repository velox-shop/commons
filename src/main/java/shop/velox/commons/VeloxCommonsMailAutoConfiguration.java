package shop.velox.commons;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.mail.MailSenderAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import shop.velox.commons.mail.service.impl.VeloxMailService;


@Configuration
@ComponentScan("shop.velox.commons")
@EnableMethodSecurity
@AutoConfigureAfter({MailSenderAutoConfiguration.class})
@ConditionalOnProperty("velox.mail.sender")
public class VeloxCommonsMailAutoConfiguration {


  @Bean("mailService")
  @ConfigurationProperties(prefix = "velox.mail")
  VeloxMailService veloxMailService(@Autowired JavaMailSender mailSender) {
    return new VeloxMailService(mailSender);
  }

}
