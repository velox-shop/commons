package shop.velox.commons;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;

@Slf4j
public abstract class AbstractVeloxWebSecurityConfiguration {

  protected HttpSecurity commonConfiguration(HttpSecurity http) throws Exception {
    logActiveConfiguration(this.getClass());
    http.sessionManagement(c -> c.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
    http.authorizeHttpRequests((authz) -> authz
        .requestMatchers("/actuator/health").permitAll()
        .requestMatchers("/actuator/**").hasAuthority("Admin")
        .requestMatchers("/**").permitAll()
    );
    http.cors(Customizer.withDefaults());
    http.csrf(AbstractHttpConfigurer::disable);
    return http;
  }

  protected void logActiveConfiguration(
      final Class<? extends AbstractVeloxWebSecurityConfiguration> clazz) {
    log.info("Active Velox web security configuration: {}", clazz.getSimpleName());
  }
}
