package shop.velox.commons.actuate;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.Instant;
import java.util.Collection;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.boot.actuate.scheduling.ScheduledTasksEndpoint;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.config.ScheduledTask;
import org.springframework.scheduling.config.ScheduledTaskHolder;
import org.springframework.scheduling.support.ScheduledMethodRunnable;
import org.springframework.web.server.ResponseStatusException;

/**
 * {@link Endpoint @Endpoint} to expose information about an application's scheduled tasks. This
 * endpoint extends the default {@link ScheduledTasksEndpoint} with a write-endpoint to start tasks
 * manually.
 */
@Endpoint(id = "scheduledtasks")
@Slf4j
public class CustomScheduledTasksEndpoint extends ScheduledTasksEndpoint {

  private final Collection<ScheduledTaskHolder> scheduledTaskHolders;

  private final TaskScheduler taskScheduler;

  public CustomScheduledTasksEndpoint(Collection<ScheduledTaskHolder> scheduledTaskHolders,
      TaskScheduler taskScheduler) {
    super(scheduledTaskHolders);
    this.scheduledTaskHolders = scheduledTaskHolders;
    this.taskScheduler = taskScheduler;
  }

  @WriteOperation
  public void startScheduledTask(@Selector String target) {
    Optional<ScheduledTask> scheduledTask = this.scheduledTaskHolders.stream()
        .map(ScheduledTaskHolder::getScheduledTasks)
        .flatMap(Collection::stream)
        .filter(scheduledTaskIter -> target.equals(
            getTarget(scheduledTaskIter.getTask().getRunnable())))
        .findFirst();
    if (scheduledTask.isPresent()) {
      taskScheduler.schedule(scheduledTask.get().getTask().getRunnable(), Instant.now());
    } else {
      log.error("Could not find task for target {}", target);
      throw new ResponseStatusException(HttpStatus.NOT_FOUND,
          "Could not find task for target " + target);
    }
  }

  protected String getTarget(Runnable runnable) {
    Runnable runnableCopy = runnable;
    // OutcomeTrackingRunnable has private access so reflection is needed.
    // See https://github.com/open-telemetry/opentelemetry-java-instrumentation/pull/12739/files
    if ("org.springframework.scheduling.config.Task$OutcomeTrackingRunnable".equals(
        runnable.getClass().getName())) {
      try {
        Field runnableField = runnable.getClass().getDeclaredField("runnable");
        runnableField.setAccessible(true);
        runnableCopy = (Runnable) runnableField.get(runnable);
      } catch (NoSuchFieldException | IllegalAccessException e) {
        log.error("Failed to access underlying runnable", e);
      }
    }
    if (runnableCopy instanceof ScheduledMethodRunnable scheduledMethodRunnable) {
      Method method = scheduledMethodRunnable.getMethod();
      return method.getDeclaringClass().getName() + "." + method.getName();
    } else {
      log.warn("Could not determine target for runnable of type {}", runnableCopy.getClass());
      return runnableCopy.getClass().getName();
    }
  }

}
