package shop.velox.commons.converter;

import static org.apache.commons.collections4.ListUtils.emptyIfNull;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public interface Converter<ENTITY, DTO> {

  DTO convertEntityToDto(ENTITY entity);

  ENTITY convertDtoToEntity(DTO dto);

  default List<DTO> convertEntityToDto(List<ENTITY> entityList) {
    return emptyIfNull(entityList)
        .stream()
        .filter(Objects::nonNull)
        .map(this::convertEntityToDto)
        .collect(Collectors.toList());
  }

  default List<ENTITY> convertDtoToEntity(List<DTO> dtoList) {
    return emptyIfNull(dtoList)
        .stream()
        .filter(Objects::nonNull)
        .map(this::convertDtoToEntity)
        .collect(Collectors.toList());
  }

  default Page<DTO> convert(Pageable pageable, Page<ENTITY> entitiesPage) {
    return new PageImpl<>(this.convertEntityToDto(entitiesPage.getContent()),
        pageable, entitiesPage.getTotalElements());
  }
}
