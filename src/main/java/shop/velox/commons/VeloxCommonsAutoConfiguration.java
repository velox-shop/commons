package shop.velox.commons;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import shop.velox.commons.security.permission.Evaluator;

@Configuration
@ComponentScan("shop.velox.commons")
@EnableMethodSecurity
@Slf4j
public class VeloxCommonsAutoConfiguration {

  @Value("${velox.websecurity.debug:false}")
  boolean webSecurityDebug;

  @Bean
  public WebSecurityCustomizer webSecurityCustomizer() {
    log.info("WebSecurityCustomizer in debug mode: {}", webSecurityDebug);
    return web -> web.debug(webSecurityDebug);
  }

  @Bean
  static MethodSecurityExpressionHandler expressionHandler(Evaluator evaluator) {
    var expressionHandler = new DefaultMethodSecurityExpressionHandler();
    expressionHandler.setPermissionEvaluator(evaluator);
    return expressionHandler;
  }

}
