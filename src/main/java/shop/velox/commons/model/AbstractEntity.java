package shop.velox.commons.model;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Temporal;
import jakarta.validation.constraints.NotNull;
import java.io.Serial;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@SuperBuilder
@Data
@Jacksonized
@NoArgsConstructor
public class AbstractEntity implements Serializable {

  @Serial
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "PK")
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
  @GenericGenerator(name = "native", strategy = "native")
  private Long pk;

  @CreatedDate
  @NotNull
  @Column(name = "CREATION_TS", nullable = false, updatable = false)
  @Temporal(jakarta.persistence.TemporalType.TIMESTAMP)
  @EqualsAndHashCode.Exclude
  private Date createTime;

  @LastModifiedDate
  @NotNull
  @Temporal(jakarta.persistence.TemporalType.TIMESTAMP)
  @Column(name = "MODIFIED_TS", nullable = false)
  @EqualsAndHashCode.Exclude
  private Date modifiedTime;

}

