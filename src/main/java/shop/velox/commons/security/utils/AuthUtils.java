package shop.velox.commons.security.utils;

import static java.util.Optional.empty;
import static shop.velox.commons.security.VeloxSecurityConstants.Oidc.ClaimName.USER_ID;

import java.security.Principal;
import java.util.Optional;
import java.util.function.Function;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

/**
 * Static class for common authentication and authorization utility methods
 */
@UtilityClass
@Slf4j
public class AuthUtils {

  private static final Function<Jwt, Optional<String>> jwtToUserIdMapper = j ->
      Optional.ofNullable(j.getClaimAsString(USER_ID));

  private static final Function<Object, Optional<String>> userToUserIdMapper = u -> switch (u) {
    case User user -> Optional.of(user.getUsername());
    case String s -> Optional.of(s);
    default -> Optional.empty();
  };

  private static final Function<JwtAuthenticationToken, Optional<String>> jwtTokenToUserIdMapper = p ->
      Optional.of(p)
          .map(JwtAuthenticationToken::getToken)
          .flatMap(jwtToUserIdMapper);

  private static final Function<OAuth2AuthenticationToken, Optional<String>> oA2TokenToUserIdMapper = p ->
      Optional.of(p)
          .map(OAuth2AuthenticationToken::getPrincipal)
          .map(u -> u.getAttribute(USER_ID));

  private static final Function<UsernamePasswordAuthenticationToken, Optional<String>> usernamePasswordTokenToUserIdMapper = p ->
      Optional.of(p)
          .map(UsernamePasswordAuthenticationToken::getPrincipal)
          .flatMap(userToUserIdMapper);

  /**
   * User ID extractor from {@link Principal} object, returns null when none can be found
   */
  public static final Function<Object, String> principalToUserIdMapper = p -> {
    Optional<String> optional = switch (p) {
      case JwtAuthenticationToken jwtToken -> jwtTokenToUserIdMapper.apply(jwtToken);
      case OAuth2AuthenticationToken oA2Token -> oA2TokenToUserIdMapper.apply(oA2Token);
      case UsernamePasswordAuthenticationToken usernamePasswordToken ->
          usernamePasswordTokenToUserIdMapper.apply(usernamePasswordToken);
      case Jwt jwt -> jwtToUserIdMapper.apply(jwt);
      case User user -> userToUserIdMapper.apply(user);
      case null -> Optional.empty();
      default -> empty();
    };
    if (optional.isEmpty()) {
      log.warn("User ID could not be determined for principal {}", p);
    }
    String result = optional.orElse(null);
    log.debug("principalToUserIdMapper with {} returns {}", p, result);
    return result;
  };
}
