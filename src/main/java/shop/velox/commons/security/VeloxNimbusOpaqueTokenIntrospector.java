package shop.velox.commons.security;

import static java.util.Objects.nonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.NimbusOpaqueTokenIntrospector;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;

/**
 * This extension of the {@link NimbusOpaqueTokenIntrospector} filters and converts authorities
 * based on a prefix.
 */
@Slf4j
public class VeloxNimbusOpaqueTokenIntrospector extends NimbusOpaqueTokenIntrospector {

  private final String authoritiesClaimName;

  public VeloxNimbusOpaqueTokenIntrospector(String introspectionUri, String clientId,
      String clientSecret, String authoritiesClaimName) {
    super(introspectionUri, clientId, clientSecret);
    this.authoritiesClaimName = authoritiesClaimName;
  }

  @Override
  public OAuth2AuthenticatedPrincipal introspect(String token) {
    OAuth2AuthenticatedPrincipal oAuth2AuthenticatedPrincipal = super.introspect(token);

    return new OAuth2IntrospectionAuthenticatedPrincipal(oAuth2AuthenticatedPrincipal.getName(),
        oAuth2AuthenticatedPrincipal.getAttributes(),
        extractAuthorities(oAuth2AuthenticatedPrincipal));
  }

  Collection<GrantedAuthority> extractAuthorities(
      OAuth2AuthenticatedPrincipal oAuth2AuthenticatedPrincipal) {
    Collection<GrantedAuthority> result = List.of();

    Object authorities = oAuth2AuthenticatedPrincipal.getAttribute(authoritiesClaimName);

    if (authorities instanceof JSONObject jsonObject) {
      result = convertJsonObject(jsonObject);
    } else if (nonNull(authorities)) {
      log.error("Could not convert authorities: [{}]", authorities);
    }

    return result;
  }

  private static Collection<GrantedAuthority> convertJsonObject(JSONObject jsonObject) {
    return jsonObject.keySet().stream()
        .map(SimpleGrantedAuthority::new)
        .collect(Collectors.toCollection(ArrayList::new));
  }

}
