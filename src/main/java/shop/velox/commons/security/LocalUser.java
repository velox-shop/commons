package shop.velox.commons.security;

import java.util.List;
import lombok.Data;
import lombok.ToString;

@Data
public class LocalUser {

    private String username;

    @ToString.Exclude
    private String password;

    private List<String> authorities;
  }
