package shop.velox.commons.security;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;

@Slf4j
public class CachingOpaqueTokenIntrospector implements OpaqueTokenIntrospector {

  private final OpaqueTokenIntrospector delegate;
  private final Cache<String, OAuth2AuthenticatedPrincipal> cache;

  public CachingOpaqueTokenIntrospector(OpaqueTokenIntrospector delegate, int cacheExpireSeconds) {
    this.delegate = delegate;
    this.cache = Caffeine.newBuilder()
        .expireAfterWrite(cacheExpireSeconds, TimeUnit.SECONDS)
        .maximumSize(100)
        .build();
    log.info("Created CachingOpaqueTokenIntrospector with expireSeconds: {}", cacheExpireSeconds);
  }

  @Override
  public OAuth2AuthenticatedPrincipal introspect(String token) {
    return cache.get(token, k -> {
      log.info("Cache miss. Introspecting token");
      return delegate.introspect(token);
    });
  }
}
