package shop.velox.commons.security.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.lang.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

@Service
public interface SecurityService {

  Optional<Authentication> getAuthentication();

  Collection<? extends GrantedAuthority> getRoles(@Nullable Authentication authentication);

  boolean hasAnyAuthority(@Nullable Authentication authentication,
      List<String> authorities);

  boolean hasAnyAuthority(List<String> authorities);

  Optional<String> getUserId();
}
