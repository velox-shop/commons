package shop.velox.commons.security.service.impl;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.collections4.ListUtils.intersection;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import shop.velox.commons.security.service.SecurityService;
import shop.velox.commons.security.utils.AuthUtils;

@Service
@Slf4j
public class SecurityServiceImpl implements SecurityService {

  @Override
  public Optional<Authentication> getAuthentication() {
    return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication());
  }

  @Override
  public Collection<? extends GrantedAuthority> getRoles(@Nullable Authentication authentication) {
    return Optional.ofNullable(authentication)
        .map(Authentication::getAuthorities)
        .orElse(Collections.emptyList());
  }

  @Override
  public boolean hasAnyAuthority(@Nullable Authentication authentication,
      List<String> authorities) {
    List<String> userRoles = getRoles(authentication).stream()
        .map(GrantedAuthority::getAuthority)
        .toList();
    boolean result = isNotEmpty(intersection(userRoles, authorities));
    log.debug("user with roles: {} has any of: {} returns: {}", userRoles, authorities, result);
    return result;
  }

  @Override
  public boolean hasAnyAuthority(List<String> authorities) {
    return getAuthentication()
        .map(authentication -> hasAnyAuthority(authentication, authorities))
        .orElse(false);
  }

  @Override
  public Optional<String> getUserId() {
    return getAuthentication()
        .map(AuthUtils.principalToUserIdMapper);
  }

}
