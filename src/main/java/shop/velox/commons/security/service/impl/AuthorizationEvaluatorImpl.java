package shop.velox.commons.security.service.impl;

import static shop.velox.commons.security.VeloxSecurityConstants.Authorities.GLOBAL_ADMIN_AUTHORIZATION;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import shop.velox.commons.security.service.AuthorizationEvaluator;
import shop.velox.commons.security.utils.AuthUtils;

@Component("veloxAuthorizationEvaluator")
@Slf4j
public class AuthorizationEvaluatorImpl implements AuthorizationEvaluator {

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean hasGlobalAdminAuthority(final Authentication authentication) {
    final Predicate<String> isAdminAuthority = a -> Objects.equals(GLOBAL_ADMIN_AUTHORIZATION, a);
    var authorities = authentication.getAuthorities();
    boolean result = authorities
        .stream()
        .map(GrantedAuthority::getAuthority)
        .anyMatch(isAdminAuthority);
    log.debug("User with authorities: {} has global admin authority: {}", authorities, result);
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean hasGlobalOrCustomAdminAuthority(final Authentication authentication,
      final String adminAuthority) {
    final Predicate<String> isAdminAuthority = a -> Arrays
        .asList(GLOBAL_ADMIN_AUTHORIZATION, adminAuthority).contains(a);
    Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
    boolean result = authorities.stream().map(GrantedAuthority::getAuthority)
        .anyMatch(isAdminAuthority);
    log.debug("User with authorities: {} has global or custom admin authority: {}",
        authorities, result);
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isCurrentUserId(final Authentication authentication, final String userId) {
    String authenticationUserId = AuthUtils.principalToUserIdMapper.apply(authentication);
    boolean result = Optional.ofNullable(authenticationUserId)
        .filter(currentId -> currentId.equals(userId)).isPresent();
    log.debug("User with id: {} is current ({}) user: {}", authenticationUserId, userId, result);
    return result;
  }
}
