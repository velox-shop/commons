package shop.velox.commons.security.service;

import org.springframework.security.core.Authentication;
import shop.velox.commons.security.VeloxSecurityConstants.Authorities;

/**
 * Velox specific authorization methods
 */
public interface AuthorizationEvaluator {

  /**
   * Checks if current user represented in authentication object has global {@link
   * Authorities#GLOBAL_ADMIN_AUTHORIZATION}
   *
   * @param authentication current authentication object
   * @return true if authentication object represents user with expected admin authority
   */
  boolean hasGlobalAdminAuthority(Authentication authentication);

  /**
   * Checks if current user represented in authentication object has global {@link
   * Authorities#GLOBAL_ADMIN_AUTHORIZATION} or service specific admin privileges
   *
   * @param authentication current authentication object
   * @param adminAuthority additional service specific authority identifier
   * @return true if authentication object represents user with at least one of expected admin
   * authorities
   */
  boolean hasGlobalOrCustomAdminAuthority(Authentication authentication,
      String adminAuthority);

  /**
   * Checks provided user ID against current authentication object.
   *
   * @param authentication current authentication object
   * @param userId         user ID for comparision
   * @return true if authentication object represents user with provided ID
   */
  boolean isCurrentUserId(Authentication authentication, String userId);
}
