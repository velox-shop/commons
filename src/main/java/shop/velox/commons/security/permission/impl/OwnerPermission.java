package shop.velox.commons.security.permission.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Function;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import shop.velox.commons.security.annotation.OwnerId;
import shop.velox.commons.security.permission.Permission;
import shop.velox.commons.security.utils.AuthUtils;

/**
 * {@link Permission implementation of 'owner' permission dentified by {@link OwnerId} annotation on
 * entity classes.}
 */
@Component("owner")
@Slf4j
public class OwnerPermission implements Permission {

  private Map<Class, BiPredicate<Object, String>> ownerMatchers = new HashMap<>();

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isAllowed(final Authentication authentication, final Object targetDomainObject) {
    log.info("Checking {} on {}", authentication, targetDomainObject);
    final BiPredicate<Object, String> targetOwnerMatcher = getOwnerMatcherForEntityClass(
        targetDomainObject.getClass());
    return Optional.ofNullable(targetDomainObject)
        .filter(o -> targetOwnerMatcher
            .test(o, AuthUtils.principalToUserIdMapper.apply(authentication)))
        .isPresent();
  }

  private static BiPredicate<Object, String> createOwnerMatcherForEntityClass(final Class entity) {
    final Optional<Method> getOwner = Arrays.stream(entity.getMethods())
        .filter(t -> t.isAnnotationPresent(OwnerId.class)).findAny();
    if (getOwner.isEmpty()) {
      log.error("Class {} is not annotated with {}", entity.getSimpleName(),
          OwnerId.class.getSimpleName());
      return (domainObject, currentUser) -> false;
    }
    final Function<Object, Optional<String>> ownerExtractor = getOwner
        .map(getOwnerMethod -> (Function<Object, Optional<String>>) domainObject -> {
          try {
            return Optional.ofNullable(getOwnerMethod.invoke(domainObject, (Object[]) null))
                .map(Object::toString);
          } catch (IllegalAccessException | InvocationTargetException e) {
            log.warn(
                "Using 'owner' permission on misconfigured entity class {}, returning false: {}",
                entity.getCanonicalName(), e);
          }
          return Optional.empty();
        })
        .orElse(domainObject -> {
          log.warn("Using 'owner' permission on not configured entity class {}, returning false",
              entity.getCanonicalName());
          return Optional.empty();
        });
    return (domainObject, currentUser) -> Optional.ofNullable(domainObject).flatMap(ownerExtractor)
        .filter(owner -> owner.equals(currentUser)).isPresent();
  }

  private BiPredicate<Object, String> getOwnerMatcherForEntityClass(final Class entity) {
    if (ownerMatchers.containsKey(entity)) {
      return ownerMatchers.get(entity);
    } else {
      final BiPredicate<Object, String> ownerMatcher = createOwnerMatcherForEntityClass(entity);
      ownerMatchers.put(entity, ownerMatcher);
      return ownerMatcher;
    }
  }
}