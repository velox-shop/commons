package shop.velox.commons.security.permission;

import org.springframework.security.core.Authentication;

/**
 * Interface for Velox specific permissions for domain objects
 */
public interface Permission {

  /**
   * Check whether user represented by authentication object has implemented permission on given
   * entity.
   *
   * @param authentication     authentication object representing user in question
   * @param targetDomainObject domain object entity to check
   * @return true if user has implemented permission (is allowed to access entity)
   */
  boolean isAllowed(Authentication authentication, Object targetDomainObject);
}
