package shop.velox.commons.security.permission;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * Velox implementation of {@link PermissionEvaluator} supporting permissions implemented with
 * {@link Permission}
 */
@Component
public class Evaluator implements PermissionEvaluator {

  private Map<String, Permission> permissions;

  public Evaluator(@Autowired Map<String, Permission> permissions) {
    this.permissions = permissions;
  }

  /**
   * Checks if user represented by authentication object has Velox {@link Permission} to provided
   * domain object {@inheritDoc}
   */
  @Override
  public boolean hasPermission(Authentication authentication, Object targetDomainObject,
      Object permission) {
    return Optional.of(permission)
        .map(this::resolvePermission)
        .map(Permission.class::cast)
        .map(p -> p.isAllowed(authentication, targetDomainObject))
        .orElse(false);
  }

  /**
   * Entity type and id based method (no entity instance) for permission check is not implemented
   * (returns always false) as generic entity repository access solution is not yet implemented.
   * {@inheritDoc}
   */
  @Override
  public boolean hasPermission(Authentication authentication, Serializable targetId,
      String targetType, Object permission) {
    return false;
  }

  private Permission resolvePermission(Object permission) {
    return Optional.of(permission)
        .filter(String.class::isInstance)
        .map(String.class::cast)
        .map(permissions::get)
        .orElseThrow(() -> new SecurityException("Unknown permission requested"));
  }
}
