package shop.velox.commons.actuate;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Modifier;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class CustomScheduledTasksEndpointTest {

  @Test
  @SneakyThrows
  @DisplayName("Verify that org.springframework.scheduling.config.Task$OutcomeTrackingRunnable has private access")
  void getTarget() {
    Class<?> clazz = Class.forName("org.springframework.scheduling.config.Task$OutcomeTrackingRunnable");

    assertTrue(Modifier.isPrivate(clazz.getModifiers()),
        "if this test fails, shop.velox.commons.actuate.CustomScheduledTasksEndpoint.getTarget can stop using reflection.");
  }
}
