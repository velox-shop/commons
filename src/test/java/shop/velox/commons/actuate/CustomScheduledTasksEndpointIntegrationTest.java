package shop.velox.commons.actuate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_ADMIN_PASSWORD;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_ADMIN_USERNAME;

import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.endpoint.web.WebEndpointProperties;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskHolder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import shop.velox.commons.VeloxLocalWebSecurityConfiguration;

@SpringBootTest(classes = {CustomScheduledTasksEndpointIntegrationTest.TestConfig.class})
@AutoConfigureMockMvc
@Slf4j
@Import({VeloxLocalWebSecurityConfiguration.class})
@TestPropertySource(properties = {
    "logging.level.org.springframework.security=DEBUG",
    "management.endpoints.web.exposure.include=*",
    "management.endpoints.web.base-path=/actuator"
})
class CustomScheduledTasksEndpointIntegrationTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private WebEndpointProperties webEndpointProperties;

  @Autowired
  private TestTask testTask;

  @BeforeEach
  void setUp() {
    testTask.reset();
  }

  @Test
  void testGetActuatorRoot() throws Exception {
    String url = webEndpointProperties.getBasePath();

    mockMvc.perform(get(url)
            .with(httpBasic(LOCAL_ADMIN_USERNAME, LOCAL_ADMIN_PASSWORD))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  void testGetBeans() throws Exception {
    String url = webEndpointProperties.getBasePath() + "/beans";

    mockMvc.perform(get(url)
            .with(httpBasic(LOCAL_ADMIN_USERNAME, LOCAL_ADMIN_PASSWORD))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  void testStartScheduledTask() throws Exception {
    String target = TestTask.class.getName() + ".run";
    String url = webEndpointProperties.getBasePath() + "/scheduledtasks/" + target;

    mockMvc.perform(post(url)
            .with(httpBasic(LOCAL_ADMIN_USERNAME, LOCAL_ADMIN_PASSWORD))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNoContent());

    // Wait for the task to be executed
    boolean executed = testTask.latch.await(5, TimeUnit.SECONDS);
    assertNotNull(executed);
    assertEquals(1, testTask.executionCount);
  }

  @Test
  void testStartScheduledTask_taskNotFound() throws Exception {
    String target = "non.existent.task";
    String url = webEndpointProperties.getBasePath() + "/scheduledtasks/" + target;

    mockMvc.perform(post(url)
            .with(httpBasic(LOCAL_ADMIN_USERNAME, LOCAL_ADMIN_PASSWORD))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @Configuration
  @EnableAutoConfiguration
  @EnableScheduling
  static class TestConfig {

    @Bean
    public CustomScheduledTasksEndpoint customScheduledTasksEndpoint(
        TaskScheduler taskScheduler, Set<ScheduledTaskHolder> scheduledTaskHolders) {
      // Inject the auto-configured ScheduledTaskHolder from the application context.
      return new CustomScheduledTasksEndpoint(scheduledTaskHolders, taskScheduler);
    }

    @Bean
    public TaskScheduler taskScheduler() {
      ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
      scheduler.setPoolSize(5);
      scheduler.setThreadNamePrefix("TestTaskScheduler-");
      scheduler.initialize();
      return scheduler;
    }

    // Make TestTask a Spring-managed bean so @Scheduled is processed.
    @Bean
    public TestTask testTask() {
      return new TestTask();
    }
  }

  @Slf4j
  static class TestTask {

    private int executionCount = 0;
    private final CountDownLatch latch = new CountDownLatch(1);

    @Scheduled(fixedRate = 100000) // Long delay so it doesn't run automatically
    public void run() {
      log.info("TestTask executed");
      executionCount++;
      latch.countDown();
    }

    public void reset() {
      executionCount = 0;
    }
  }
}
