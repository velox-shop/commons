package shop.velox.commons.security.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static shop.velox.commons.security.VeloxSecurityConstants.Oidc.ClaimName.USER_ID;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

@Slf4j
public class AuthUtilsTest {

  @Nested
  class PrincipalToUserIdMapperTest {

    @Test
    void UsernamePasswordAuthenticationTokenTest() {
      // Given
      String externalId = "externalId";
      Authentication authentication = new UsernamePasswordAuthenticationToken(
          externalId,
          null,
          List.of(new SimpleGrantedAuthority("User")));

      // When
      String actualExternalId = AuthUtils.principalToUserIdMapper.apply(authentication);

      // Then
      assertEquals(externalId, actualExternalId);
    }

    @Test
    void JwtAuthenticationTokenTest() {
      // Given
      String externalId = "externalId";
      Map<String, Object> headers = Map.of("dummyHeader", "dummyValue");
      Map<String, Object> claims = Map.of(USER_ID, externalId);
      JwtAuthenticationToken authentication = new JwtAuthenticationToken(
          new Jwt(externalId, Instant.now(), Instant.now().plusSeconds(60), headers, claims));

      log.info(authentication.getPrincipal().getClass().getName());

      // When
      String actualExternalId = AuthUtils.principalToUserIdMapper.apply(authentication);

      // Then
      assertEquals(externalId, actualExternalId);
    }

    @Test
    void OAuth2AuthenticationTokenTest() {
      // Given
      String externalId = "externalId";
      Map<String, Object> attributes = Map.of(USER_ID, externalId);
      OAuth2AuthenticationToken authentication = new OAuth2AuthenticationToken(
          new DefaultOAuth2User(List.of(new SimpleGrantedAuthority("User")), attributes, USER_ID),
          List.of(new SimpleGrantedAuthority("User")),
          "registrationId");

      // When
      String actualExternalId = AuthUtils.principalToUserIdMapper.apply(authentication);

      // Then
      assertEquals(externalId, actualExternalId);
    }

    @Test
    void JwtTest() {
      // Given
      String externalId = "externalId";
      Map<String, Object> headers = Map.of("dummyHeader", "dummyValue");
      Map<String, Object> claims = Map.of(USER_ID, externalId);
      Jwt jwt = new Jwt(externalId, Instant.now(), Instant.now().plusSeconds(60), headers, claims);

      // When
      String actualExternalId = AuthUtils.principalToUserIdMapper.apply(jwt);

      // Then
      assertEquals(externalId, actualExternalId);
    }

    @Test
    void UserTest() {
      // Given
      String externalId = "externalId";
      User user = new User(externalId, "password", List.of(new SimpleGrantedAuthority("User")));

      // When
      String actualExternalId = AuthUtils.principalToUserIdMapper.apply(user);

      // Then
      assertEquals(externalId, actualExternalId);
    }

  }
}
