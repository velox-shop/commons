package shop.velox.commons.security;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

import java.util.List;
import java.util.Map;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.DefaultOAuth2AuthenticatedPrincipal;

@ExtendWith(MockitoExtension.class)
class VeloxNimbusOpaqueTokenIntrospectorTest {

  private static final String AUTHORITIES_CLAIM_NAME = "urn:zitadel:iam:org:project:roles";

  private VeloxNimbusOpaqueTokenIntrospector veloxNimbusOpaqueTokenIntrospector;

  @BeforeEach
  void setUp() {
    veloxNimbusOpaqueTokenIntrospector = new VeloxNimbusOpaqueTokenIntrospector("uri", "clientId",
        "clientSecret", AUTHORITIES_CLAIM_NAME);
  }

  @Test
  void extractAuthorities() {
    // Given
    var adminRole = "Admin";
    Map<String, Object> attributes = Map.of(AUTHORITIES_CLAIM_NAME, new JSONObject(Map.of(
        adminRole, new JSONObject(Map.of("195449414855164161", "velox.zitadel.cloud")))));

    DefaultOAuth2AuthenticatedPrincipal auth2AuthenticatedPrincipal = new DefaultOAuth2AuthenticatedPrincipal(
        attributes, List.of());

    // When
    var result = veloxNimbusOpaqueTokenIntrospector.extractAuthorities(auth2AuthenticatedPrincipal);

    // Then
    assertThat(result, contains(new SimpleGrantedAuthority(adminRole)));
  }

}
