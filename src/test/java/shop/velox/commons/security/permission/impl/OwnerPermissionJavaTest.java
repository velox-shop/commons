package shop.velox.commons.security.permission.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Random;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import shop.velox.commons.security.annotation.OwnerId;

@ExtendWith(MockitoExtension.class)
@Slf4j
class OwnerPermissionJavaTest {

  static String DEFAULT_OWNER = "A default owner" + new Random().nextLong();
  OwnerPermission permission = new OwnerPermission();
  Object domainObject = new DomainObject(DEFAULT_OWNER);

  @Test
  @DisplayName("Authentication is accepted as JwtAuthenticationToken class")
  void jwtAuthenticationTokenIsAcceptedTest() {
    // Given
    Jwt token = Mockito.mock(Jwt.class);
    Mockito.when(token.getClaimAsString(Mockito.anyString())).thenReturn(DEFAULT_OWNER);
    JwtAuthenticationToken auth = Mockito.spy(new JwtAuthenticationToken(token));
    Mockito.when(auth.toString()).thenCallRealMethod();

    // When
    boolean permissionAllowed = permission.isAllowed(auth, domainObject);

    //Then
    assertTrue(permissionAllowed);

  }

  @Test
  @DisplayName("Authentication is accepted as OAuth2AuthenticationToken class")
  void oAuth2AuthenticationTokenIsAcceptedTest() {
    // Given
    OAuth2User oAuth2User = Mockito.mock(OAuth2User.class);
    Mockito.when(oAuth2User.getAttribute(Mockito.anyString())).thenReturn(DEFAULT_OWNER);
    OAuth2AuthenticationToken auth = Mockito.spy(
        new OAuth2AuthenticationToken(oAuth2User, List.of(), "foo"));
    Mockito.when(auth.toString()).thenCallRealMethod();

    // When
    boolean permissionAllowed = permission.isAllowed(auth, domainObject);

    // Then
    assertTrue(permissionAllowed);

  }

  @ParameterizedTest(name = "{index} => Permission verification result for {0} to entity owned by {1} should be {2}")
  @CsvSource({
      "1,               1,              true",
      "me@velox.local,  me@velox.local, true ",
      "me@velox.local,  1,              false",
      "me2@velox.local, me@velox.local, false",
      "me@velox.local,  ,               false",
      ",                me@velox.local, false"
  })
  void test(String principal, String entityOwner, boolean result) {
    // Given
    Jwt token = Mockito.mock(Jwt.class);
    Mockito.when(token.getClaimAsString(Mockito.anyString())).thenReturn(principal);
    JwtAuthenticationToken auth = Mockito.spy(new JwtAuthenticationToken(token));
    Mockito.when(auth.toString()).thenCallRealMethod();

    // When
    boolean permissionAllowed = permission.isAllowed(auth, new DomainObject(entityOwner));

    // Then
    assertEquals(result, permissionAllowed);
  }


  static class DomainObject {

    String OWNER;

    DomainObject(String owner) {
      this.OWNER = owner;
    }

    @OwnerId
    public String getOwnerId() {
      return OWNER;
    }
  }

}
