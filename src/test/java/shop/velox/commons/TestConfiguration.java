package shop.velox.commons;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Import;

@SpringBootConfiguration
@Import(VeloxCommonsMailAutoConfiguration.class)
@EnableAutoConfiguration
public class TestConfiguration {

}