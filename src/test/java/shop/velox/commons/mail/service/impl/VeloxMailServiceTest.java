package shop.velox.commons.mail.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.ActiveProfiles;
import shop.velox.commons.TestConfiguration;
import shop.velox.commons.mail.service.MailService;

@SpringBootTest(classes = TestConfiguration.class)
@ActiveProfiles("mailTest")
class VeloxMailServiceTest {

  @Autowired(required = false)
  private MailService mailService;

  @Autowired(required = false)
  private JavaMailSender javaMailSender;

  @Test
  void testVeloxMailServiceBeanCreation() {
    assertThat(mailService).isNotNull();
  }

  @Test
  void testJavaMailSenderBeanCreation() {
    assertThat(javaMailSender).isNotNull();
  }
}
