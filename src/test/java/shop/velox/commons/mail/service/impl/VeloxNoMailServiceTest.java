package shop.velox.commons.mail.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import shop.velox.commons.TestConfiguration;
import shop.velox.commons.mail.service.MailService;

@SpringBootTest(classes = TestConfiguration.class)
class VeloxNoMailServiceTest {

  @Autowired(required = false)
  private MailService mailService;

  @Autowired(required = false)
  private JavaMailSender javaMailSender;

  @Test
  void testVeloxMailServiceBeanCreation() {
    assertThat(mailService).isNull();
  }

  @Test
  void testJavaMailSenderBeanCreation() {
    assertThat(javaMailSender).isNull();
  }
}
