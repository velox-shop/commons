package shop.velox.commons.utils;

import java.net.URI;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

class QueryParamsUtilsTest {

  @Test
  void addQueryParamsTest() {
    // Given
    var uriComponentsBuilder = UriComponentsBuilder.fromUriString("http://localhost:8080");
    var queryParams = new LinkedMultiValueMap<String, String>();
    queryParams.add("firstName", "myNäme");
    queryParams.add("email", "foo+bar@example.com");

    // When
    QueryParamsUtils.addQueryParams(uriComponentsBuilder, queryParams);

    // Then
    var actual = uriComponentsBuilder.build(true).toUri();
    assertThat(actual).extracting(URI::toString).isEqualTo(
        "http://localhost:8080?firstName=myN%C3%A4me&email=foo%2Bbar%40example.com");
  }
}
