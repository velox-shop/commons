package shop.velox.commons.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTimeout;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.annotation.XmlRootElement;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import lombok.Data;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

class JaxbUtilsTest {

  @Data
  @XmlRootElement
  public static class SimpleEntity {

    private String content;
  }

  @Test
  void unmarshal_whenXXEAttempted_shouldNotResolveExternalEntity() throws Exception {
    // Arrange
    JAXBContext context = JAXBContext.newInstance(SimpleEntity.class);

    // Create a temp file to attempt to read via XXE
    Path tempFile = Files.createTempFile("secret", ".txt");
    Files.write(tempFile, "secret data".getBytes());

    // XML with XXE attack payload
    String maliciousXml = String.format("""
        <?xml version="1.0" encoding="UTF-8"?>
        <!DOCTYPE foo [
            <!ENTITY xxe SYSTEM "file://%s">
        ]>
        <simpleEntity>
            <content>&xxe;</content>
        </simpleEntity>
        """, tempFile.toAbsolutePath());

    try {
      // Act
      Object result = JaxbUtils.unmarshal(context, maliciousXml);
      SimpleEntity entity = (SimpleEntity) result;

      // Assert
      assertNotNull(entity);
      // Content should not contain the file contents
      String entityContent = entity.getContent();
      assertThat(entityContent).doesNotContain("secret data");
      // Content should either be empty or contain the literal "&xxe;"
      assertThat(entityContent)
          .matches(content -> content == null || content.isEmpty() || content.equals("&xxe;"),
              "Content should either be null, empty, or '&xxe;'");

    } finally {
      // Cleanup
      Files.deleteIfExists(tempFile);
    }
  }

  @Test
  void unmarshal_withValidXml_shouldSucceed() throws Exception {
    // Arrange
    JAXBContext context = JAXBContext.newInstance(SimpleEntity.class);
    String validXml = """
        <?xml version="1.0" encoding="UTF-8"?>
        <simpleEntity>
            <content>valid content</content>
        </simpleEntity>
        """;

    // Act
    Object result = JaxbUtils.unmarshal(context, validXml);
    SimpleEntity entity = (SimpleEntity) result;

    // Assert
    assertNotNull(entity);
    assertEquals("valid content", entity.getContent());
  }

  @Test
  @SneakyThrows
    /* The Billion Laughs attack (also known as XML Entity Expansion attack) is a denial-of-service attack that exploits XML entity expansion.
     * Each level multiplies the previous level by 10, creating exponential growth.
     * If this continues for more levels, it can create strings billions of characters long from a tiny XML document, consuming all available memory.
     * This test asserts that even with a potentially explosive entity expansion, the parsing completes in under 2 seconds.
     * If entity expansion were unlimited, this would take much longer or crash with an OutOfMemoryError.
     *
     */
  void unmarshal_withBillionLaughsAttack_shouldNotCauseDoS() {
    // Arrange
    JAXBContext context = JAXBContext.newInstance(SimpleEntity.class);
    String billionLaughsXml = """
        <?xml version="1.0" encoding="UTF-8"?>
        <!DOCTYPE lolz [
            <!ENTITY lol "lol">
            <!ENTITY lol2 "&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;">
            <!ENTITY lol3 "&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;">
            <!ENTITY lol4 "&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;">
            <!ENTITY lol5 "&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;">
        ]>
        <simpleEntity>
            <content>&lol5;</content>
        </simpleEntity>
        """;

    // Act & Assert
    assertTimeout(Duration.ofSeconds(2), () -> {
      Object result = JaxbUtils.unmarshal(context, billionLaughsXml);
      assertNotNull(result);
    });
  }
}
